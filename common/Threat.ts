import ScaleValue from "./ScaleValue";
import { v4 as uuid } from "uuid";

export default class Threat {
    id: string = uuid();
    name: string;
    damage?: ScaleValue;
    reproducibility?: ScaleValue;
    exploitability?: ScaleValue;
    affectedUsers?: ScaleValue;
    discoverability?: ScaleValue;
    dreadRating?: number;

    constructor(name: string, damage?: ScaleValue, reproducibility?: ScaleValue, 
        exploitability?: ScaleValue, affectedUsers?: ScaleValue, discoverability?: ScaleValue) {
        this.name = name;
        this.damage = damage;
        this.reproducibility = reproducibility;
        this.exploitability = exploitability;
        this.affectedUsers = affectedUsers;
        this.discoverability = discoverability;
        this.updateDreadRating();
    }

    static fromObject(object: any): Threat {
        if (!object.name) {
            throw new Error("Threat name cannot be empty");
        }

        const threat = new Threat(object.name, object.damage, object.reproducibility, object.exploitability,
             object.affectedUsers, object.discoverability);
        if (object.id) {
            threat.id = object.id;
        }

        threat.updateDreadRating();
        return threat;
    }

    updateDreadRating(): void {
        if (!this.damage || !this.reproducibility || !this.exploitability ||
            !this.affectedUsers || !this.discoverability) {
                this.dreadRating = undefined;
                return;
        }

        this.dreadRating = (this.damage.value + this.reproducibility.value + this.exploitability.value
             + this.affectedUsers.value + this.discoverability.value) / 5;
    }
};