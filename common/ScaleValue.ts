import Scale from "./Scale";

export default class {
    //scale: Scale; // Is this really needed? Avoid circular dependencies for now
    id: number;
    name: string;
    value: number;

    constructor(scale: Scale, id: number, name: string, value: number) {
        //this.scale = scale;
        this.id = id;
        this.name = name;
        this.value = value;
    }
}