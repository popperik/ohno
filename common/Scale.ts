import ScaleValue from "./ScaleValue";

export default class {
    id: number;
    name: string;
    values: ScaleValue[];

    constructor(id: number, name: string, values: ScaleValue[] = []) {
        this.id = id;
        this.name = name;
        this.values = values;
    }
}