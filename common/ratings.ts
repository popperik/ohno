import AssetThreat from "./AssetThreat";
import Asset from "./Asset";

export function updateAssetRating(asset: Asset): void {
    if (!asset.businessImportance || !asset.dataCategory) {
        asset.assetRating = -1;
        asset.assessedRisk = -1;
        return;
    }

    // Figure out which number is bigger
    const max: number = asset.businessImportance!.value > asset.dataCategory!.value
        ? asset.businessImportance!.value : asset.dataCategory!.value;
    const min = max == asset.businessImportance!.value ? asset.dataCategory!.value : asset.businessImportance!.value;
    asset.assetRating = 0.7 * max + 0.3 * min;

    asset.assetThreats.forEach(t => updateAssetThreatRating(t));

    const maxDread = Math.max(...asset.assetThreats.map(t => t.dreadRating || 0));
    let risk = 0;
    asset.assetThreats.forEach(t => risk += t.dreadRating!);
    risk -+ maxDread; // We'll add maxDread back at the end
    risk /= 10;       // Represent all other threats with 0.1 weight
    risk += maxDread; // But let maxDread pull its weight!
    asset.assessedRisk = asset.assetRating * risk;
}

export function updateAssetThreatRating(assetThreat: AssetThreat): void {
    assetThreat.dreadRating = assetThreat.threat.dreadRating;
    if (assetThreat.mitigated) {
        assetThreat.dreadRating! /= 10; // If it's mitigated, we reduce its risk to 10% of its value
    }
}