import { AssetType } from "./AssetType";
import { v4 as uuid } from "uuid";
import ScaleValue from "./ScaleValue";
import AssetThreat from "./AssetThreat";
import { updateAssetRating } from "./ratings";

export default class Asset {
    id = uuid();
    name: string;
    type: AssetType;
    attackTree: string = "";
    businessImportance?: ScaleValue;
    dataCategory?: ScaleValue;
    assetRating: number = -1;
    assessedRisk: number = -1;
    assetThreats: AssetThreat[] = [];

    constructor(name: string, type: AssetType, businessImportance?: ScaleValue, dataCategory?: ScaleValue) {
        this.name = name;
        this.type = type;
        this.businessImportance = businessImportance;
        this.dataCategory = dataCategory;

        updateAssetRating(this);
    }

    static fromObject(object: any): Asset {
        if (!object.name) {
            throw new Error("Asset name cannot be empty");
        }

        if (!object.type) {
            throw new Error("Asset type cannot be empty");
        }

        const asset = new Asset(object.name, object.type);
        if (object.businessImportance) {
            asset.businessImportance = object.businessImportance;
        }

        if (object.dataCategory) {
            asset.dataCategory = object.dataCategory;
        }

        if (object.id) {
            asset.id = object.id;
        }

        if (object.assetThreats) {
            asset.assetThreats = object.assetThreats.map((t: any) => AssetThreat.fromObject(t));
        }

        if (object.attackTree) {
            asset.attackTree = object.attackTree;
        }

        updateAssetRating(asset);
        return asset;
    }
};