import { v4 as uuid } from "uuid";
import Threat from "./Threat";
import { updateAssetThreatRating } from "./ratings";

export default class AssetThreat {
    id: string = uuid();
    threat: Threat;
    mitigated: boolean;
    dreadRating?: number;

    constructor(threat: Threat, mitigated: boolean = false) {
        this.threat = threat;
        this.mitigated = mitigated;
        updateAssetThreatRating(this);
    }

    static fromObject(object: any): AssetThreat {
        const assetThreat = new AssetThreat(Threat.fromObject(object.threat),
            object.mitigated);
        
        if (object.id) {
            assetThreat.id = object.id;
        }

        updateAssetThreatRating(assetThreat);
        return assetThreat;
    }
}