export enum AssetType {
    Product = "Product",
    Feature = "Feature",
    Component = "Component",
    Library = "Library"
};