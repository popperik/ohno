# Oh no! 😱

## Development setup

First, install the dependencies:

```bash
npm install
```

Then, spin up the database (it's PostgreSQL in a Docker container):

```bash
./run-db.sh
```

And finally, run Oh no! 😱

```bash
./run.sh
```