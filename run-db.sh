#!/bin/bash

docker run --rm --name ohno-db -p 5432:5432 \
    -e POSTGRES_USER=ohno \
    -e POSTGRES_DB=ohno \
    -e POSTGRES_PASSWORD=DUMMY \
    postgres
