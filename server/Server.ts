import express from "express";
import http from "http";
import * as controller from "./Controller";

export default class {
    staticRoot: string;
    port: number = 8080;
    private server = http.createServer();
    private app = express();
    
    constructor(staticRoot: string) {
        this.server.on("request", this.app); // Bind Express to HTTP server
        const server = this.server;
        this.staticRoot = staticRoot;
        this.registerRoutes();
    }

    start(): void {
        this.server.listen(this.port, () => console.log("Oh no! is up and running!😱"));
    }

    private registerRoutes(): void {
        this.app.use(express.json({type: "*/*"})); // Use middleware to automatically parse incoming request bodies
        controller.registerRoutes(this.app);
        this.app.use(express.static(this.staticRoot));
        this.app.get("/", (req, res) => res.sendFile("/index.html", {root: this.staticRoot}));
    }
}
