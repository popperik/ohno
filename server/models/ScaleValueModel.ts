import { PrimaryColumn, Column, ManyToOne, Entity } from "typeorm";
import { ScaleModel } from "./ScaleModel";

@Entity("scaleValues")
export class ScaleValueModel {

    @PrimaryColumn()
    public id!: number;

    @Column()
    public name!: string;

    @Column()
    public value!: number;

    @ManyToOne(type => ScaleModel, scale => scale.scaleValues)
    public scale!: ScaleModel;
}
