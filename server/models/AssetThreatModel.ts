import { Entity, PrimaryColumn, Column, ManyToOne } from "typeorm";
import { ThreatModel } from "./ThreatModel";
import { AssetModel } from "./AssetModel";

@Entity("assetThreats")
export class AssetThreatModel {

    @PrimaryColumn()
    public id!: string;

    @Column()
    public assetId!: string;

    @Column()
    public threatId!: string;

    @Column()
    public mitigated!: boolean;

    @ManyToOne(() => AssetModel, asset => asset.assetThreats)
    public asset!: AssetModel;

    @ManyToOne(() => ThreatModel, threat => threat.assetThreats)
    public threat?: ThreatModel;
}
