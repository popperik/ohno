import { Entity, PrimaryColumn, Column, ManyToOne, OneToMany } from "typeorm";
import { ScaleValueModel } from "./ScaleValueModel";
import { AssetThreatModel } from "./AssetThreatModel";

@Entity("threats")
export class ThreatModel {

    @PrimaryColumn()
    public id!: string;

    @Column()
    public name!: string;

    @ManyToOne(type => ScaleValueModel, scaleValue => scaleValue.id)
    public damage!: ScaleValueModel;

    @ManyToOne(type => ScaleValueModel, scaleValue => scaleValue.id)
    public reproducibility!: ScaleValueModel;

    @ManyToOne(type => ScaleValueModel, scaleValue => scaleValue.id)
    public exploitability!: ScaleValueModel;

    @ManyToOne(type => ScaleValueModel, scaleValue => scaleValue.id)
    public affectedUsers!: ScaleValueModel;

    @ManyToOne(type => ScaleValueModel, scaleValue => scaleValue.id)
    public discoverability!: ScaleValueModel;

    @OneToMany(type => AssetThreatModel, assetThreat => assetThreat.threat)
    public assetThreats!: AssetThreatModel[]
}