import { Entity, PrimaryColumn, Column, ManyToOne, OneToMany } from "typeorm";
import { ScaleValueModel } from "./ScaleValueModel";
import { AssetThreatModel } from "./AssetThreatModel";

@Entity("assets")
export class AssetModel {

    @PrimaryColumn()
    public id!: string;

    @Column()
    public type!: string;

    @Column()
    public name!: string;

    @Column()
    public attackTree!: string;

    @ManyToOne(() => ScaleValueModel, scaleValue => scaleValue.id)
    public businessImportance!: ScaleValueModel;

    @ManyToOne(() => ScaleValueModel, scaleValue => scaleValue.id)
    public dataCategory?: ScaleValueModel;

    @OneToMany(() => AssetThreatModel, assetThreat => assetThreat.asset, { cascade: true })
    public assetThreats?: AssetThreatModel[];
}
