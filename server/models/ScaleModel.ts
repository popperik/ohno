import { Entity, PrimaryColumn, Column, OneToMany } from "typeorm";
import { ScaleValueModel } from "./ScaleValueModel";

@Entity("scales")
export class ScaleModel {

    @PrimaryColumn()
    public id!: number;

    @Column()
    public name!: string;

    @OneToMany(type => ScaleValueModel, scaleValue => scaleValue.scale)
    public scaleValues?: ScaleValueModel[];
}