import {MigrationInterface, QueryRunner} from "typeorm";
import fs from "fs";

export class devdata20200721091209 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        queryRunner.query(fs.readFileSync(__dirname + "/20200721091209-devdata-up.sql", "utf8"));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }
}
