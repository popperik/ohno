CREATE TABLE "scales"(
    "id" SERIAL PRIMARY KEY,
    name TEXT NOT NULL
);

INSERT INTO "scales" ("id", "name") VALUES
    (1, 'Business importance'),
    (2, 'Data category'),
    (3, 'Damage'),
    (4, 'Reproducibility'),
    (5, 'Exploitability'),
    (6, 'Affected users'),
    (7, 'Discoverability');

CREATE TABLE "scaleValues"(
    "id" SERIAL PRIMARY KEY,
    "scaleId" INT NOT NULL REFERENCES "scales",
    "name" TEXT NOT NULL,
    "value" REAL NOT NULL
);

INSERT INTO "scaleValues" ("scaleId", "name", "value") VALUES
    (1, 'Business critical', 1),
    (1, 'Product critical', 0.7),
    (1, 'Important', 0.3),
    (1, 'No direct business impact', 0.01),
    (2, 'Loss of customers', 1),
    (2, 'Major issue', 0.6),
    (2, 'Inconvenience', 0.2),
    (2, 'Nothing', 0.01),
    (3, 'Critical hit', 1),
    (3, 'Major impact', 0.8),
    (3, 'Minor impact', 0.3),
    (3, 'Miss', 0.01),
    (4, 'Always', 1),
    (4, 'Most of the time', 0.8),
    (4, 'Circumstantial', 0.5),
    (4, 'Once in a blue moon', 0.1),
    (4, 'Never', 0.01),
    (5, 'EZPZ', 1),
    (5, 'Prep work', 0.9),
    (5, 'Hard work', 0.6),
    (5, 'Depends on other issues', 0.2),
    (5, 'Theoretical', 0.01),
    (6, 'All', 1),
    (6, 'Potentially all', 0.8),
    (6, 'Limited', 0.5),
    (6, 'Very few', 0.1),
    (6, 'None', 0.01),
    (7, 'Trivial', 1),
    (7, 'Well-known', 0.8),
    (7, 'Hole-in-the-wall', 0.4),
    (7, 'Hidden', 0.2),
    (7, 'Impossible', 0.01);

CREATE TABLE "assets"(
    "id" UUID PRIMARY KEY,
    "name" TEXT NOT NULL,
    "type" TEXT NOT NULL, -- TODO add types table
    "attackTree" TEXT,
    --createdBy TEXT, -- Ideally this would be a foreign key, but we don't have the data yet
    "businessImportanceId" INT REFERENCES "scaleValues",
    "dataCategoryId" INT REFERENCES "scaleValues"
);

CREATE TABLE "threats"(
    "id" UUID PRIMARY KEY,
    "name" TEXT NOT NULL,
    "damageId" INT NOT NULL REFERENCES "scaleValues",
    "reproducibilityId" INT NOT NULL REFERENCES "scaleValues",
    "exploitabilityId" INT NOT NULL REFERENCES "scaleValues",
    "affectedUsersId" INT NOT NULL REFERENCES "scaleValues",
    "discoverabilityId" INT NOT NULL REFERENCES "scaleValues"
);

CREATE TABLE "assetThreats"(
    "id" UUID PRIMARY KEY,
    "assetId" UUID REFERENCES "assets" ON DELETE CASCADE ON UPDATE CASCADE,
    "threatId" UUID REFERENCES "threats" ON DELETE CASCADE ON UPDATE CASCADE,
    "mitigated" BOOLEAN DEFAULT FALSE
);