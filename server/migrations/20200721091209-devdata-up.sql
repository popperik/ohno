create extension "uuid-ossp";

INSERT INTO assets ("id", "name", "type", "businessImportanceId", "dataCategoryId") VALUES
    (uuid_generate_v4(), 'Security portal', 'Component', 4, 7),
    (uuid_generate_v4(), 'Translations', 'Library', 3, 8),
    (uuid_generate_v4(), 'SAML', 'Feature', 1, 7);

INSERT INTO threats ("id", "name", "damageId", "reproducibilityId", "exploitabilityId", "affectedUsersId", "discoverabilityId") VALUES
    (uuid_generate_v4(), 'XSS',  9, 14, 20, 25, 29),
    (uuid_generate_v4(), 'DDoS', 9, 14, 21, 24, 29),
    (uuid_generate_v4(), 'Session prediction', 9, 16, 21, 25, 29);

INSERT INTO "assetThreats" ("id", "assetId", "threatId") VALUES
    (uuid_generate_v4(), (SELECT id FROM assets WHERE name = 'Security portal'), (SELECT id FROM threats WHERE name = 'XSS')),
    (uuid_generate_v4(), (SELECT id FROM assets WHERE name = 'Security portal'), (SELECT id FROM threats WHERE name = 'DDoS')),
    (uuid_generate_v4(), (SELECT id FROM assets WHERE name = 'Translations'), (SELECT id FROM threats WHERE name = 'DDoS')),
    (uuid_generate_v4(), (SELECT id FROM assets WHERE name = 'SAML'), (SELECT id FROM threats WHERE name = 'DDoS'));