import {MigrationInterface, QueryRunner} from "typeorm";
import fs from "fs";

export class init20200718183007 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        queryRunner.query(fs.readFileSync(__dirname + "/20200718183007-init-up.sql", "utf8"));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }
}
