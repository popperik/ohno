import "reflect-metadata";
import Server from "./Server";
import { createConnection } from "typeorm";

(async () => await createConnection({
    type: "postgres",
    host: process.env.PGHOST,
    port: Number(process.env.PGPORT),
    username: process.env.PGUSER,
    password: process.env.PGPASSWORD,
    database: process.env.PGDATABASE,
    entities: [ __dirname + "/models/*.js"],
    logging: true,
    migrationsRun: true,
    migrations: [ __dirname + "/migrations/*.js"]
}).catch(error => console.log(`Failed to initialise database: ${error}`)))();

const server = new Server("dist/client");
server.start();
