import express from "express";
import Asset from "../common/Asset";
import Threat from "../common/Threat";
import Scale from "../common/Scale";
import ScaleValue from "../common/ScaleValue";
import { getRepository } from "typeorm";
import { ScaleModel } from "./models/ScaleModel";
import { AssetModel } from "./models/AssetModel";
import { ThreatModel } from "./models/ThreatModel";
 
async function getAssetsHandler(req: express.Request, res: express.Response) {
    const rows = await getRepository(AssetModel).find({ relations: ["businessImportance", "dataCategory", 
        "assetThreats", "assetThreats.threat", "assetThreats.threat.damage", "assetThreats.threat.reproducibility",
        "assetThreats.threat.exploitability", "assetThreats.threat.affectedUsers",
        "assetThreats.threat.discoverability"] });
    const assets = rows.map(r => Asset.fromObject(r));
    res.json(assets);
}

async function getScalesHandler(req: express.Request, res: express.Response) {
    const rows = await getRepository(ScaleModel).find({ relations: ["scaleValues"]});
    const scales = rows.map(r => {
        const scale = new Scale(r.id, r.name);
        scale.values = r.scaleValues!.map(v => new ScaleValue(scale, v.id, v.name, v.value));
        return scale;
    });
    res.json(scales);
}

function saveAssetHandler(req: express.Request, res: express.Response): void {
    try {
        const asset = Asset.fromObject(req.body);
        console.info(`Saving asset: ${JSON.stringify(asset)}`);

        getRepository(AssetModel).save(asset).then(() => res.sendStatus(204));
    } catch (e) {
        res.status(400).send(e.message);
    }
}

function deleteAssetHandler(req: express.Request, res: express.Response): void {
    console.info(`Deleting asset: ${req.params.id}`);

    getRepository(AssetModel).delete(req.params.id)
        .then(() => res.sendStatus(204))
        .catch(e => res.status(400).send(e.message));
}

async function getThreatsHandler(req: express.Request, res: express.Response): Promise<void> {
    const rows = await getRepository(ThreatModel).find({ relations: 
        ["damage", "reproducibility", "exploitability", "affectedUsers", "discoverability"] });
    const threats = rows.map(r => Threat.fromObject(r));
    res.json(threats);
}

function saveThreatHandler(req: express.Request, res: express.Response): void {
    try {
        const threat = Threat.fromObject(req.body);
        console.info(`Saving threat: ${JSON.stringify(threat)}`);
        getRepository(ThreatModel).save(threat).then(() => res.sendStatus(204));
    } catch (e) {
        res.status(400).send(e.message);
    }
}

function deleteThreatHandler(req: express.Request, res: express.Response): void {
    console.info(`Deleting threat: ${req.params.id}`);

    getRepository(ThreatModel).delete(req.params.id)
        .then(() => res.sendStatus(204))
        .catch(e => res.status(400).send(e.message));
}

export function registerRoutes(app: express.Application): void {
    app.get("/asset", getAssetsHandler);
    app.put("/asset", saveAssetHandler);
    app.delete("/asset/:id", deleteAssetHandler);
    app.get("/scale", getScalesHandler);
    app.get("/threat", getThreatsHandler);
    app.put("/threat", saveThreatHandler);
    app.delete("/threat/:id", deleteThreatHandler);
}