import Asset from "../common/Asset";
import Threat from "../common/Threat";
import Scale from "../common/Scale";

export interface OhnoStore {
    assets: Asset[],
    threats: Threat[],
    scales: Scale[]
}

const store: OhnoStore = {
    assets: [],
    threats: [],
    scales: []
};

export default store;