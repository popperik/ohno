import Vue from "vue";
import VueRouter from "vue-router";
import { BootstrapVue } from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-vue/dist/bootstrap-vue.min.css";
import main from "./main";
import AssetClassification from "./views/AssetClassification";
import ThreatLibrary from "./views/ThreatLibrary";
import ThreatModeling from "./views/ThreatModeling";

Vue.use(VueRouter);
Vue.use(BootstrapVue);

const router = new VueRouter({
    routes: [
        { path: "/", component: AssetClassification },
        { path: "/threatModeling", component: ThreatModeling },
        { path: "/threatlib", component: ThreatLibrary },
    ]
});

const app = new Vue({
    el: "#app",
    router,
    render: h => h(main)
});