import store from "./store";
import ScaleValue from "../common/ScaleValue";

export function getScaleValues(scaleName: string): ScaleValue[] {
    const scale = store.scales?.find(s => s.name == scaleName);        
    if (!scale) {
        return [];
    }

    return scale.values;
}

export function getScaleValue(scaleName: string, id: number): ScaleValue | undefined {
    return getScaleValues(scaleName).find(v => v.id == id);
}

export function getScaleValuesForSelect(scaleName: string): any[] {
    return getScaleValues(scaleName).map(v => { return { text: v.name, value: v.id }});
}