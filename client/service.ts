import store from "./store";
import Asset from "../common/Asset";
import Threat from "../common/Threat";
import Scale from "../common/Scale";

export function getAssets(): Promise<Asset[]> {
    return fetch("/asset")
        .then(result => result.json())
        .then(assets => store.assets = assets);
}

export function deleteAsset(asset: Asset): Promise<void> {
    return fetch(`/asset/${asset.id}`, {method: "DELETE"})
        .then(() => { return; });
}

export function saveAsset(asset: Asset): Promise<void> {
    return fetch("/asset", {method: "PUT", body: JSON.stringify(asset)})
        .then(() => { return; });
}

export function getScales(): Promise<Scale[]> {
    return fetch("/scale")
        .then(result => result.json())
        .then(scales => store.scales = scales);
}

export function getThreats(): Promise<Threat[]> {
    return fetch("/threat")
        .then(result => result.json())
        .then(threats => store.threats = threats);
}

export function deleteThreat(threat: Threat): Promise<void> {
    return fetch(`/threat/${threat.id}`, {method: "DELETE"})
        .then(() => { return; });
}

export function saveThreat(threat: Threat): Promise<void> {
    return fetch("/threat", {method: "PUT", body: JSON.stringify(threat)})
        .then(() => { return; });
}